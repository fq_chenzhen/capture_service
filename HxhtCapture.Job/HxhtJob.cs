﻿using Common.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HxhtCapture.Job
{
    public class HxhtJob : IJob
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(HxhtJob));
        public void Execute(IJobExecutionContext context)
        {
            Megaeyes.Instance.WaitForLogin().Result.CaptureAllJpeg();
        }


    }
}

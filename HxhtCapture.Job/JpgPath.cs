﻿using Infrastructure.Lib.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HxhtCapture.Job
{
    public class JpgPath
    {
        public static string TempFileNameWithoutExt = Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString("N"));

        public static string GenerateFilePath(string stcd)
        {
            var d = Path.Combine(Megaeyes.Instance.BasePath, stcd, DateTime.Now.ToString("yyyMM"),
                 DateTime.Now.ToString("yyy-MM-dd"));

            if (!FileHelper.IsExistDirectory(d))
            {
                FileHelper.CreateDirectory(d);
            }

            return Path.Combine(d, DateTime.Now.ToString("yyyMMdd_HHmmss") + ".jpg");
        }

    }
}

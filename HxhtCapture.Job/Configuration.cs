﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HxhtCapture.Job
{

    [XmlRoot("configuration")]
    public class Configuration
    {
        [XmlElement("saveBasePath")]
        public string SaveBasePath { get; set; }


        [XmlElement("host")]
        public string Host { get; set; }


        [XmlElement("port")]
        public int Port { get; set; }


        [XmlElement("name")]
        public string Name { get; set; }


        [XmlElement("pswd")]
        public string Pswd { get; set; }


        [XmlArray("cameraList"), XmlArrayItem("camera")]
        public List<Camera> CameraList { get; set; }

    }

    public class Camera
    {
        [XmlAttribute("id")]
        public string Stcd { get; set; }

        [XmlAttribute("title")]
        public string Title { get; set; }

        [XmlAttribute("naming")]
        public string Naming { get; set; }
    }
}

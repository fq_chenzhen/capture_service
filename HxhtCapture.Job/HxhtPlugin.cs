﻿using Common.Logging;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HxhtCapture.Job
{
    public class HxhtPlugin : ISchedulerPlugin
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(HxhtPlugin));

        public void Initialize(string pluginName, Quartz.IScheduler sched)
        {
            logger.Warn("实例化");
        }

        public void Shutdown()
        {
            logger.Warn("关闭");
        }

        public void Start()
        {
            Megaeyes.Instance.Login();
            logger.Warn("启动");
        }
    }
}

﻿using com_errorLib;
using com_realtime_playerLib;
using com_session_ptzctrlLib;
using com_sessionLib;
using Common.Logging;
using Infrastructure.Lib.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HxhtCapture.Job
{
    public partial class Megaeyes
    {
        //static void Main(string[] args)
        //{
        //    Megaeyes.Instance.Login();
        //}
        private static readonly ILog logger = LogManager.GetLogger(typeof(Megaeyes));

        private Configuration _config { get; set; }

        private HxhtSessionClass _hxhtSession;

        private string _account_naming { get; set; }

        private string _account { get; set; }

        private int _account_level { get; set; }

        private string _session_id { get; set; }

        private string _j2ee_ip { get; set; }

        private int _j2ee_port { get; set; }

        private string _center_ip { get; set; }

        private int _center_port { get; set; }

        private bool _isLogined = false;


        private string _initXml
        {
            get
            {
                return string.Format(@"<Message type=""center"">
                                        <Center host=""{0}"" port=""{1}""/>
                                        <Account name=""{2}"" pswd=""{3}""/>
                                    </Message>", _config.Host, _config.Port, _config.Name, _config.Pswd);
            }
        }


        public string BasePath {
            get {
                return _config.SaveBasePath;
            }
        }

        private string get_cfg_str(string naming, string title)
        {
            var cfg_str = "";
            cfg_str += "<Source type=\"access\">";
            cfg_str += "<Camera naming=\"" + naming + "\" name=\"" + title + "\" />";
            cfg_str += "<Version value=\"1\" />";
            cfg_str += "<Account naming=\"" + _account_naming + "\" name=\"" + _account + "\" privilege=\"" + _account_level + "\" sessionid=\"" + _session_id + "\" />";
            cfg_str += "<Center ip=\"" + _j2ee_ip + "\" port=\"" + _j2ee_port + "\" />";
            cfg_str += "<Access ip=\"" + _center_ip + "\" port=\"" + _center_port + "\" />";
            cfg_str += "</Source>";

            return cfg_str;
        }

        private Megaeyes()
        {
            var xmlPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "HxhtConfig.xml");
            _config = SerializeHelper.FromXmlFile<Configuration>(xmlPath);
        }

        public static Megaeyes Instance
        {
            get { return MegaeyesInstance.instance; }
        }

        class MegaeyesInstance
        {
            internal static readonly Megaeyes instance = new Megaeyes();

            static MegaeyesInstance()
            {

            }
        }

    }
}

﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Infrastructure.Lib.Core;

namespace FetchApiDataOfWholeally
{
    public class FetchOpenApiJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger<FetchOpenApiJob>();

        //private string addr = "15671";
        //private HttpHelper httpReader = new HttpHelper("http://vp.wholeally.net:18081");
        //private HttpHelper httpWriter = new HttpHelper("http://120.26.82.157:8053");
        //访问直播链接防止过时
        //private HttpHelper httpHlsAccessor = new HttpHelper("http://101.132.152.150:8234/1000005951001_2_0/online.m3u8");

        public void Execute(IJobExecutionContext context)
        {
            try
            {

                ConfigurationInfo.Intance.Devices.ForEach(d =>
                {
                    var httpFetch = new HttpHelper(d.FetchBaseUrl);
                    var httpPost = new HttpHelper(d.PostBaseUrl);

                    var result = httpFetch.Post(
                        new {device_serial = d.DeviceSerial}
                        , new Dictionary<string, string>()
                        {
                            {"Accept", "application/json"},
                            {"Authorization", d.Authorization}
                        }
                        , d.FetchApiSegment);

                    var RetObj = result.JsonToModels<dynamic>();

                    if ((int) RetObj.ret != -1)
                    {
                        var sen_list = (IEnumerable<dynamic>) RetObj.sen_list;

                        var z_result = from s in sen_list where (int)s.type == 15 select s;

                        foreach (var sen in z_result)
                        {
                            httpPost.Post(new
                            {
                                d.DeviceSerial,
                                sen.type,
                                sen.addr,
                                data = Convert.ToDecimal(sen.waterLevel) / 1000,
                                deviceTime = Convert.ToDateTime(sen.time)
                            }, d.PostApiSegment);
                        }
                    }

                });
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }


            //var result = httpReader.Post(new { device_serial = "FS0018AP01" }, new Dictionary<string, string>()
            //    {
            //        {"Accept", "application/json"},
            //        {"Authorization", "2df8d766-cd2a-11e8-9ed8-00163e041f45"}
            //    }, "/openapi/sensor/data");

            //var obj = result.JsonToModels<dynamic>();

            //obj = ("{\"ret\":0,\"msg\":\"Ok\",\"sen_list\":[{\"type\":1,\"addr\":\"15671\",\"z\":\"" + Math.Round(RandomHelper.NextDouble() * 100, 2) + "\"}]}")
            //        .JsonToModels<dynamic>();

            //if ((int)obj.ret != -1)
            //{
            //    var list = obj.sen_list as IEnumerable<dynamic>;

            //    //查找指定站点的数据
            //    var r1 = from l in list where (string)l.addr == addr select l.z;

            //    var data = (string)r1.ToList()[0];

            //    //获取数据
            //    httpWriter.Post(new { addr = "123", data = data }, "/api/WholeallyWater");
            //}

            //httpHlsAccessor.Get(null, "");

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Infrastructure.Lib.Core;

namespace FetchApiDataOfWholeally
{
    public class ConfigurationInfo
    {
        private readonly static Lazy<Configuration> _instance = new Lazy<Configuration>(() =>
        {
            var xmlPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "WholeallyFetchApi.xml");
            return SerializeHelper.FromXmlFile<Configuration>(xmlPath);
        });

        public static Configuration Intance
        {
            get { return _instance.Value; }
        }

        ConfigurationInfo()
        { }
    }


    [XmlRoot("configuration")]
    public class Configuration
    {

        [XmlArray("devices"), XmlArrayItem("device")]
        public List<Device> Devices { get; set; }
    }

    public class Device
    {
        [XmlAttribute("fetchBaseUrl")]
        public string FetchBaseUrl { get; set; }

        [XmlAttribute("fetchApiSegment")]
        public string FetchApiSegment { get; set; }

        [XmlAttribute("authorization")]
        public string Authorization { get; set; }

        [XmlAttribute("deviceSerial")]
        public string DeviceSerial { get; set; }

        [XmlAttribute("postBaseUrl")]
        public string PostBaseUrl { get; set; }

        [XmlAttribute("postApiSegment")]
        public string PostApiSegment { get; set; }

    }
}

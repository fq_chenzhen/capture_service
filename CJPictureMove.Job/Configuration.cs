﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CJPictureMove.Job
{
    [XmlRoot("configuration")]
    public class Configuration
    {
        [XmlElement("from")]
        public string From { get; set; }

        [XmlElement("to")]
        public string To { get; set; }

        [XmlArray("sites"), XmlArrayItem("site")]
        public List<Site> Sites { get; set; }
    }

    public class Site
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

    }

}

﻿using Common.Logging;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CJPictureMove.Job
{
    public class PictrueMoveJobPlugin : ISchedulerPlugin
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PictrueMoveJobPlugin));

        public void Initialize(string pluginName, Quartz.IScheduler sched)
        {
            FolderDeamon.Instance.Watch();

            logger.Warn("实例化");
        }

        public void Shutdown()
        {
            logger.Warn("关闭");
        }

        public void Start()
        {
            logger.Warn("启动");
        }
    }
}

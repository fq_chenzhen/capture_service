﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HCCapture;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using DHNetSDK;

namespace DHCapture.Job
{
    public class DHCammera : CammeraBase
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(DHCammera));

        /// <summary>
        /// 登录
        /// </summary>
        /// <returns></returns>
        public async Task LogInAsync()
        {
            await Task.Factory.StartNew(() =>
            {
                logger.InfoFormat("{0} begin logining...", DvInfo.IP);

                NET_DEVICEINFO deviceInfo = new NET_DEVICEINFO();
                int error = 0;

                this.lLoginID = DHClient.DHLogin(DvInfo.IP, DvInfo.PORT, DvInfo.UID, DvInfo.PWD, out deviceInfo, out error);

                if (lLoginID > 0)
                {
                    this.Active = true;//this.LogIn(ref msg);
                    logger.InfoFormat("{0} login success", DvInfo.IP);
                }
                else
                {
                    this.Active = false;
                    logger.InfoFormat("{0} login failed", DvInfo.IP);

                }
            });
        }

        /// <summary>
        /// 抓图
        /// </summary>
        /// <param name="isUpload"></param>
        public async void SnapPictureAsync()
        {
            await Task.Factory.StartNew(() =>
            {
                logger.InfoFormat("{0} begin snap...", DvInfo.IP);
                SNAP_PARAMS snapparams = new SNAP_PARAMS();
                snapparams.Channel = 0;
                snapparams.mode = 0;
                snapparams.CmdSerial = 1;
                snapparams.Quality = 1;

                bool bRet = DHClient.DHSnapPicture(lLoginID, snapparams);
                if (!bRet)
                {
                    lLoginID = -1;
                    logger.InfoFormat("{0} 抓图启动失败", DvInfo.IP);
                }

                logger.InfoFormat("{0} end snap...", DvInfo.IP);
            });
        }

        public void SaveLocal(byte[] buf, uint revLen)
        {
            string jpgfileName = this.DvInfo.ID + System.DateTime.Now.ToString("yyMMddHHmmss");
            string sJpegPicFileName = string.Format(@"{0}\{1}.jpg", this.CapJpgPath.TrimEnd(new char[] { '/', '\\' }), jpgfileName);

            this.CreateLocalPath();
            using (System.IO.FileStream fs = System.IO.File.Create(sJpegPicFileName))
            {
                fs.Write(buf, 0, (int)revLen);
            }
            logger.InfoFormat("saveed local: {0}", jpgfileName);
        }

        public void SaveFtp(byte[] buf, uint revLen)
        {
            string jpgfileName = this.DvInfo.ID + System.DateTime.Now.ToString("yyMMddHHmmss") + ".jpg";
            //ftp 上传
            FtpWeb ftp = new FtpWeb(DevConfig.ftpClient.IP, "", DevConfig.ftpClient.UID, DevConfig.ftpClient.PWD);
            this.CreateFtpPath(ftp);
            ftp.Upload(jpgfileName, buf, Convert.ToInt32(revLen));

            logger.InfoFormat("saveed ftp: {0}", jpgfileName);
        }

        // <summary>
        /// 转到预置点
        /// </summary>
        /// <param name="msg"></param>
        public bool DHPTZControl(ushort index)
        {
            if (lLoginID <= 0)
            {
                return false;
            }

            logger.InfoFormat("{0} begin PTZPreset...", DvInfo.IP);
            var mrtn = DHClient.DHPTZControl(lLoginID, DvInfo.Channel, PTZ_CONTROL.PTZ_POINT_MOVE_CONTROL, index/*预置点值*/, false);
            if (!mrtn)
            {
                lLoginID = -1;
            }
            logger.InfoFormat("{0} end PTZPreset...", DvInfo.IP);
            return mrtn;
        }

        /// <summary>
        //登出设备
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool LogOut()
        {
            //注销登录 Logout the device
            if (lLoginID >= 0)
            {
                DHClient.DHLogout(lLoginID);
                lLoginID = -1;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

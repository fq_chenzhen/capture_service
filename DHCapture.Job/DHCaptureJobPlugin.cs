﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz.Spi;
using Common.Logging;

namespace DHCapture.Job
{
    public class DHCaptureJobPlugin : ISchedulerPlugin
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(DHCaptureJobPlugin));
        public void Initialize(string pluginName, Quartz.IScheduler sched)
        {
            logger.Warn("实例化");
        }

        public async void Start()
        {
            logger.Warn("启动");
            try
            {
                DHCaptureJob.DHInitSDK();

                if (!DHCaptureJob.IsLoginAll)
                {
                    await DHCaptureJob.LoginALLAsync();
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }

        public void Shutdown()
        {
            logger.Warn("关闭");
            //退出登录
            try
            {
                DHCaptureJob.DisposeAll();
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }
    }
}

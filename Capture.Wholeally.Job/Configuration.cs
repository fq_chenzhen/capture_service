﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Infrastructure.Lib.Core;

namespace Capture.Wholeally.Job
{

    public class ConfigurationInfo
    {
        private readonly static Lazy<Configuration> _instance = new Lazy<Configuration>(() =>
        {
            var xmlPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "WholeallyCaptureJob.xml");
            return SerializeHelper.FromXmlFile<Configuration>(xmlPath);
        });

        public static Configuration Instatnce
        {
            get { return _instance.Value; }
        }

        ConfigurationInfo()
        { }
    }


    [XmlRoot("configuration")]
    public class Configuration
    {

        [XmlElement("baseUrl")]
        public string BaseUrl { get; set; }


        [XmlElement("shotUrl")]
        public string ShotUrl { get; set; }

        [XmlElement("authorization")]
        public string Authorization { get; set; }

        [XmlElement("account")]
        public string Account { get; set; }

        [XmlElement("saveImagePath")]
        public string SaveImagePath { get; set; }

        [XmlArray("sites"), XmlArrayItem("site")]
        public List<Site> Sites { get; set; }
    }

    public class Site
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("deviceSerial")]
        public string DeviceSerial { get; set; }

        [XmlAttribute("chanNo")]
        public string ChanNo { get; set; }

    }
}

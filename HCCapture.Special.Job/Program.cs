﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HCCapture.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            Test();
            Console.WriteLine("Current Thread Id 1:{0}", Thread.CurrentThread.ManagedThreadId);
            Console.Read();
        }
        static void Test()
        {
            //设备初始化
            string msg = "";
            CameraManger.InitCammerList(ref msg);
            Console.WriteLine("before login 0");

            //Console.WriteLine("Current Thread Id :{0}", Thread.CurrentThread.ManagedThreadId);

            //await Task.Factory.StartNew(async () =>
            // {
            //     Console.WriteLine("Current Thread Id 2:{0}", Thread.CurrentThread.ManagedThreadId);
            //     await GetName();
            // });

            //await CaptureJob.LoginALLAsync();
            //Console.WriteLine("Current Thread Id 4:{0}", Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine("after login 1");
            //CaptureJob.CaptureJpegAll();

        }

        static async Task GetName()
        {
            // Delay 方法来自于.net 4.5 
            await Task.Factory.StartNew(() => 
             {
                 Thread.Sleep(1000);
                 ;  // 返回值前面加 async 之后，方法里面就可以用await了 
                 Console.WriteLine("Current Thread Id 3:{0}", Thread.CurrentThread.ManagedThreadId);
                 Console.WriteLine("In antoher thread.....");
             });

        }

    }
}

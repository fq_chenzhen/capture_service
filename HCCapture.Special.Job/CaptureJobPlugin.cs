﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz.Spi;
using Common.Logging;

namespace HCCapture.Special.Job
{
    public class CaptureJobPlugin : ISchedulerPlugin
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(CaptureJob));
        public void Initialize(string pluginName, Quartz.IScheduler sched)
        {
            logger.Warn("实例化");
        }

        public void Start()
        {
            logger.Warn("启动");

            DevConfig.LoadConfig();

            CameraManger.CammeraList = new List<Cammera>();

            foreach (var d in DevConfig.devList)
            {
                CameraManger.CammeraList.Add(new Cammera()
                {
                    Active = false,
                    ID = d.ID,
                    IP = d.IP,
                    PORT = d.PORT,
                    UID = d.UID,
                    PWD = d.PWD,
                    Channel = d.Channel,
                    PresetIndex = d.PresetIndex,
                    PrePresetIndex = d.PrePresetIndex,
                    CapJpgPath = DevConfig.CapJpgPath,
                    CaptureInterval = DevConfig.CaptureInterval
                });
            }

            logger.Info("begin InitCammerList...");

            CHCNetSDK.NET_DVR_SetLogToFile(3, ".\\SdkLog\\", true);            
            var m_bInitSDK = CHCNetSDK.NET_DVR_Init();

            logger.Info("end InitCammerList...");

            if (m_bInitSDK == false)
            {
                logger.Fatal("NET_DVR_Init error!");
            }
            else
            {
                //保存SDK日志 To save the SDK log
                CHCNetSDK.NET_DVR_SetLogToFile(3, ".\\SdkLog\\", true);
            }
            /*
            try
            {
                //登录海康摄像头
                if (!CaptureJob.IsLoginAll)
                {
                    if (null == CameraManger.CammeraList)
                    {    //设备初始化
                        string msg = "";
                        logger.Info("begin InitCammerList...");
                        CameraManger.InitCammerList(ref msg);
                        logger.Info("end InitCammerList...");
                    }
                    //判断登录情况
                    //已经登录就不用登录
                    await CaptureJob.LoginALLAsync();
                    CaptureJob.IsLoginAll = true;

                    var query = from c in CameraManger.CammeraList
                                where c.Active
                                select c;

                    CaptureJob.ActiveCammerList = query.ToList();
                    logger.Info("find active cammer list...");
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
             */
        }

        public void Shutdown()
        {
            logger.Warn("关闭");
            //退出登录
            try
            {
                CaptureJob.DisposeAll();
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }
    }
}

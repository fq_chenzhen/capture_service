﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;

namespace HCCapture
{
    public partial class Cammera
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Cammera));
        public async Task LogInAsync()
        {
            await Task.Factory.StartNew(() => 
             {
                 logger.InfoFormat("{0} begin logining...", this.IP);
                 string msg = "";
                 this.Active = this.LogIn(ref msg);
                 //Console.WriteLine(this.IP + " login");
                 //Console.WriteLine("Current Thread Id :{0}", Thread.CurrentThread.ManagedThreadId);
                 logger.InfoFormat("{0} {1}", this.IP, msg);
                 //return this.Active;
             });
        }

        public async Task CaptureJpegAsync(bool isUpload = false)
        {
            await Task.Factory.StartNew(() => 
             {
                 logger.InfoFormat("{0} begin capture...", this.IP);
                 string msg = "";
                 msg = this.CaptureJpeg(isUpload);
                 logger.Info(this.IP + msg);
             });
        }

        public async Task CaptureJpeg_NEWAsync(bool isUpload = false)
        {
            await Task.Factory.StartNew(() => 
             {
                 logger.InfoFormat("{0} begin capture...", this.IP);
                 string msg = "";
                 msg = this.CaptureJpeg_NEW();
                 //Console.WriteLine(this.IP + msg);
                 //logger.InfoFormat("Current Thread Id :{0}", Thread.CurrentThread.ManagedThreadId);
                 logger.InfoFormat("{0} {1}", this.IP, msg);
             });

            //Console.WriteLine("Capture");
        }

    }
}

﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Quartz;
using HCCapture;
using System.Collections.Generic;

namespace HCCapture.Job
{
    public class CaptureJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(CaptureJob));

        public static bool IsLoginAll = false;

        public static List<Cammera> ActiveCammerList = new List<Cammera>();

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                //等待登录成功
                if (!IsLoginAll)
                {
                    logger.Info("wait for all login success...");
                    while (!IsLoginAll)
                    {
                        Thread.Sleep(500);
                    }
                    logger.Info("all login success begin execute job...");
                }

                CaptureJob.CaptureJpegAll();
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }

        public async static Task LoginALLAsync()
        {
            //Console.WriteLine("before login 1");

            //Console.WriteLine("Current Thread Id :{0}", Thread.CurrentThread.ManagedThreadId);



            logger.Info("begin all login...");
            //CameraManger.CammeraList.ForEach(async c=>await c.LogInAsync());

            foreach (var c in CameraManger.CammeraList)
            {
                //var msg = "";
                await c.LogInAsync();
                //c.LogIn(ref msg);
            }

            //await new Cammera() { IP="0.0.0.0"  }.LogInAsync();

            //Console.WriteLine("after login 0");
            //Console.WriteLine("Current Thread Id :{0}", Thread.CurrentThread.ManagedThreadId); 


            logger.Info("end all login...");

        }

        /// <summary>
        /// 抓取所有图片
        /// </summary>
        private static void CaptureJpegAll()
        {
            logger.Info("begin capture foreach...");
            foreach (var c in ActiveCammerList)
            {
                var IsCaptureReady = true;

                if (c.PresetIndex > 0)
                {
                    logger.InfoFormat("{0} begin PTZPreset...", c.IP);
                    IsCaptureReady = c.PTZPreset_Other(c.PresetIndex);
                    Thread.Sleep(6000);
                    logger.InfoFormat("{0} end PTZPreset...", c.IP);
                }
                
                if (IsCaptureReady)
                {
                    //--模式 0 本地，1 ftp，2 本地和ftp
                    switch (DevConfig.Mode)
                    {
                        case SaveMode.仅本地:
                            c.CaptureJpegAsync();
                            break;
                        case SaveMode.仅FTP:
                            c.CaptureJpeg_NEWAsync();
                            break;
                        case SaveMode.本地和FTP:
                            c.CaptureJpegAsync(true);
                            break;
                        default:
                            c.CaptureJpegAsync();
                            break;
                    }
                }

                if (c.PrePresetIndex > 0)
                {
                    logger.InfoFormat("{0} begin PrePTZPreset...", c.IP);
                    c.PTZPreset_Other(c.PrePresetIndex);
                    //Thread.Sleep(3000);
                    logger.InfoFormat("{0} end PrePTZPreset...", c.IP);
                }
                //c.CaptureJpeg();
                //Thread.Sleep(1000);
            }
            logger.Info("end capture foreach..");
        }

        public static void DisposeAll()
        {
            logger.Info("begin dispose all...");
            ActiveCammerList.ForEach(d =>
            {
                string msg = "";
                d.LogOut(ref msg);
                d.Dispose();
            });
            logger.Info("end dispose all...");
        }
    }
}
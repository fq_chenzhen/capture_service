﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HCCapture
{
    public partial class JPGCAP : Form
    {
        public JPGCAP()
        {
            InitializeComponent();

            //设备初始化
            string msg = "";
            CameraManger.InitCammerList(ref msg);
            if (!string.IsNullOrWhiteSpace(msg))
                MessageBox.Show(msg);
        }

        private void JPGCAP_Load(object sender, EventArgs e)
        {
            lblCycle.Text = DevConfig.IntervalInfo;
            lblLocalPath.Text = DevConfig.CapJpgPath;
            lblIP.Text = "ftp://" + DevConfig.ftpClient.IP + "/";
            lblFtpPath.Text = DevConfig.ftpClient.Path;
            string strmode = DevConfig.Mode.ToString();
            //switch(DevConfig.Mode)
            //{
            //    case 0:
            //        strmode ="仅本地保存";
            //        break;
            //    case 1:
            //        strmode ="仅FTP上传";
            //        break;
            //    case 2:
            //        strmode ="本地和FTP";
            //        break;
            //}

            lblFtp.Text = strmode;
            lblTimeNow.Text = "登录设备中...";
            

            //登录设备
            var loginAction = new Action(() =>
            {
                string loginMsg = "";
                CameraManger.LoginALL(ref loginMsg);

                //显示登录信息
                this.BeginInvoke(new Action(() =>
                {
                    //清空登录状态信息
                    //lblTimeNow.Text = DevConfig.IntervalInfo + "后开始抓取";
                    //lblMsg.Text = loginMsg;
                    if (!string.IsNullOrEmpty(loginMsg))
                    {
                        //MessageBox.Show(loginMsg);
                    }
                }));
            });
            //异步调用登录
            var asyncResult = loginAction.BeginInvoke(null, null);

            //阻塞线程
            loginAction.EndInvoke(asyncResult);

            List<IntPtr> IntPtrList = new List<IntPtr>();

            //定时抓取jpeg图片
            //CameraManger.CapAll();

            //马上抓取
            new Action(() =>
            {
                this.BeginInvoke(new Action(() =>
                {
                    this.lblTimeNow.Text = System.DateTime.Now.ToString();
                }));

                CameraManger.CaptureJpegAll();
            }).BeginInvoke(null,null);

            

            var CapTimer = new System.Timers.Timer();
            CapTimer.Interval = DevConfig.CaptureInterval;
            CapTimer.Enabled = true;
            CapTimer.Elapsed += (obj, ev) =>
            {
                //阻塞线程，直到所有登录完成
                asyncResult.AsyncWaitHandle.WaitOne();

                this.BeginInvoke(new Action(() => {
                    //显示抓取时间
                    this.lblTimeNow.Text = System.DateTime.Now.ToString();
                }));

                CameraManger.CaptureJpegAll();
            };
            CapTimer.Start();
        }

        private void JPGCAP_FormClosed(object sender, FormClosedEventArgs e)
        {
            CameraManger.StopPreviewAll();
            CameraManger.LogOutAll();
            CameraManger.DisposeAll();
        }

        private void JPGCAP_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)  //判断是否最小化
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(5000, "系统提示", "程序已经最小化,正在后台运行中.", ToolTipIcon.Info);
                this.Visible = false;
            }
            else
            {
                notifyIcon1.Visible = false;
                this.Visible = true;
            }

            //this.Hide();
            //this.ShowInTaskbar = false;
            //notifyIcon1.Visible = true;
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            notifyIcon1.Visible = false;
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace HCCapture
{
    public class CameraManger
    {
        public static List<Cammera> CammeraList { get; set; }

        public static List<Thread> ThreadList { get; set; }

        /// <summary>
        /// 初始化所有设备
        /// </summary>
        public static void InitCammerList(ref string msg)
        {
            DevConfig.LoadConfig();

            CammeraList = new List<Cammera>();

            foreach (var d in DevConfig.devList)
            {
                CammeraList.Add(new Cammera()
                {
                    Active = false,
                    ID = d.ID,
                    IP = d.IP,
                    PORT = d.PORT,
                    UID = d.UID,
                    PWD = d.PWD,
                    Channel = d.Channel,
                    PresetIndex = d.PresetIndex,
                    PrePresetIndex = d.PrePresetIndex,
                    CapJpgPath = DevConfig.CapJpgPath,
                    CaptureInterval = DevConfig.CaptureInterval
                });
            }

            var m_bInitSDK = CHCNetSDK.NET_DVR_Init();
            if (m_bInitSDK == false)
            {
                //msg = this.ID + ":NET_DVR_Init error!";
                //return;
            }
            else
            {
                //保存SDK日志 To save the SDK log
                CHCNetSDK.NET_DVR_SetLogToFile(3, ".\\SdkLog\\", true);
            }

            foreach (var c in CammeraList)
            {

                c.Initialize(ref msg);
            }
        }

        /// <summary>
        /// 登录所有
        /// </summary>
        public static void LoginALL(ref string msg)
        {
            foreach (var c in CammeraList)
            {
                c.LogIn(ref msg);
            }
        }


        /// <summary>
        /// 登出所有
        /// </summary>
        public static void LogOutAll()
        {
            foreach (var c in CammeraList)
            {
                string msg = "";
                c.LogOut(ref msg);
            }
        }

        /// <summary>
        /// 预览所有设备
        /// </summary>
        /// <param name="handle"></param>
        /// <param name="msg"></param>
        public static void PreviewAll(List<IntPtr> handle)
        {

            //var thread = new Thread(() =>
            //    {
            for (int i = 0; i < CammeraList.Count; i++)
            {
                string msg = "";
                int j = i;
                CammeraList[j].Preview(handle[j], ref msg);
            }
            //});
            //设置是否为后台线程：
            //  前台线程：所有前台线程执行结束后，该进程才会关闭退出（主线程和通过Thread类创建的线程默认是前台线程）
            //  后台线程：所有前台结束后，后台线程就会立即结束(不管是否执行完成都会结束)
            //thread.IsBackground = true;
            //thread.Start();//开启线程，不传递参数
        }

        /// <summary>
        /// 停止预览所有设备
        /// </summary>
        /// <returns></returns>
        public static void StopPreviewAll()
        {
            foreach (var c in CammeraList)
            {
                c.StopPreview();
            }
        }


        public static void CapAll()
        {
            ThreadList = new List<Thread>();

            foreach (var c in CammeraList)
            {
                var thread = new Thread(() =>
                {
                    while (true)
                    {
                        c.CaptureJpeg();
                        Thread.Sleep(1000);
                    }
                });
                thread.IsBackground = true;
                thread.Start();//开启线程，不传递参数
            }
        }

        /// <summary>
        /// 抓取所有图片
        /// </summary>
        public static void CaptureJpegAll()
        {
            //foreach (var c in CammeraList)
            //{
            //    var CapTimer = new System.Timers.Timer();
            //    CapTimer.Interval = DevConfig.CaptureInterval;
            //    CapTimer.Enabled = true;
            //    CapTimer.Elapsed += (obj, ev) =>
            //    {
            //        c.CaptureJpeg();
            //    };
            //}
            //ThreadList = new List<Thread>();

            //var thread = new Thread(() =>
            //{

            foreach (var c in CammeraList)
            {
                if (c.PresetIndex > 0)
                {
                    c.PTZPreset_Other(c.PresetIndex);
                    Thread.Sleep(3000);
                }
                //--模式 0 本地，1 ftp，2 本地和ftp
                switch (DevConfig.Mode)
                {
                    case SaveMode.仅本地:
                        c.CaptureJpeg();
                        break;
                    case SaveMode.仅FTP:
                        c.CaptureJpeg_NEW();
                        break;
                    case SaveMode.本地和FTP:
                        c.CaptureJpeg(true);
                        break;
                    default:
                        c.CaptureJpeg();
                        break;
                }

                if (c.PrePresetIndex > 0)
                {
                    c.PTZPreset_Other(c.PrePresetIndex);
                    Thread.Sleep(3000);
                }
                //c.CaptureJpeg();
                //Thread.Sleep(1000);
            }

            //});

            //thread.IsBackground = true;
            //thread.Start();//开启线程，不传递参数
            //ThreadList.Add(thread);


            //foreach (var c in CammeraList)
            //{

            //    var thread = new Thread(() =>
            //        {
            //            while (true)
            //            {
            //                c.CaptureJpeg();
            //                Thread.Sleep(1000);
            //            }
            //        });
            //    //设置是否为后台线程：
            //    //  前台线程：所有前台线程执行结束后，该进程才会关闭退出（主线程和通过Thread类创建的线程默认是前台线程）
            //    //  后台线程：所有前台结束后，后台线程就会立即结束(不管是否执行完成都会结束)
            //    thread.IsBackground = true;
            //    thread.Start();//开启线程，不传递参数
            //    ThreadList.Add(thread);
            //}
        }


        /// <summary>
        /// 释放所有资源
        /// </summary>
        public static void DisposeAll()
        {
            foreach (var c in CammeraList)
            {
                c.Dispose();
            }
        }
    }
}

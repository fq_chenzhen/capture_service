﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;
using DotNet.Utilities;

namespace HCCapture
{
    public partial class PreviewList : Form
    {
        public PreviewList()
        {
            InitializeComponent();

            //FtpWeb ftp = new FtpWeb("120.26.82.157:8100", "", "strong", "strong9889");
            //if (!ftp.DirectoryExist("JPEGGRAP"))
            //    ftp.MakeDir("JPEGGRAP");
            //ftp.GotoDirectory("JPEGGRAP", false);
            //if (!ftp.DirectoryExist("01Dev"))
            //    ftp.MakeDir("01Dev");
            //ftp.GotoDirectory("01Dev", false);
            //if (!ftp.DirectoryExist("161202"))
            //    ftp.MakeDir("161202");
            //ftp.GotoDirectory("161202", false);
            //ftp.Upload(@"./JPEG/01dev/1612/161201161317.jpg");

            //FTPHelper ftp = new FTPHelper("120.26.82.157:8100", @"/", "strong", "strong9889");
            //ftp.MakeDir("Test");
            //ftp.Upload(@"./JPEG/01dev/1612/161201161317.jpg");
            
            //ftp.GetFileList("/qzbq");

            //设备初始化
            string msg = "";
            CameraManger.InitCammerList(ref msg);
            if (!string.IsNullOrWhiteSpace(msg))
                MessageBox.Show(msg);
        }

        private void PreviewList_Load(object sender, EventArgs e)
        {
            //登录设备
            string loginMsg = "";
            CameraManger.LoginALL(ref loginMsg);
            if (!string.IsNullOrEmpty(loginMsg))
            {
                MessageBox.Show(loginMsg);
            }

            List<IntPtr> IntPtrList = new List<IntPtr>();

            //预览设备
            for (int i = 0; i < CameraManger.CammeraList.Count; i++)
            {
                var pb = new System.Windows.Forms.PictureBox();
                pb.Size = new System.Drawing.Size(192, 144);
                pb.BackColor = System.Drawing.SystemColors.WindowText;
                this.flp.Controls.Add(pb);
                IntPtrList.Add(pb.Handle);
            }

            this.BeginInvoke(new Action(() =>
            {
                CameraManger.PreviewAll(IntPtrList);
            }));

             //定时抓取jpeg图片
            //CameraManger.CapAll();

            CameraManger.CaptureJpegAll();

            //var CapTimer = new System.Timers.Timer();
            //CapTimer.Interval = DevConfig.CaptureInterval;
            //CapTimer.Enabled = true;
            //CapTimer.Elapsed += (obj, ev) =>
            //{
            //    CameraManger.CaptureJpegAll();
            //};
            //CapTimer.Start();
        }

        private void PreviewList_FormClosed(object sender, FormClosedEventArgs e)
        {
            CameraManger.StopPreviewAll();
            CameraManger.LogOutAll();
            CameraManger.DisposeAll();
        }
    }
}

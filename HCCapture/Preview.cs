﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace HCCapture
{
    public partial class Preview : Form
    {
        private System.Timers.Timer CapTimer;
        private Cammera mCammeraMoniter;

        public Preview()
        {
            //DevConfig.LoadConfig();
            InitializeComponent();

            string iniMsg = "";

            //设备初始化
            mCammeraMoniter = new Cammera(ref iniMsg);
            if (!string.IsNullOrEmpty(iniMsg)) MessageBox.Show(iniMsg);
        }

        private void Prview_Load(object sender, EventArgs e)
        {
            //Control.CheckForIllegalCrossThreadCalls = false;

            //登录设备
            string loginMsg = "";
            if (mCammeraMoniter.LogIn(ref loginMsg))
            {
                label1.Text = loginMsg;
            }
            else
            {
                MessageBox.Show(loginMsg);
                this.Close();
            }

            //预览设备
            string msg = "";
            mCammeraMoniter.Preview(RealPlayWnd.Handle, ref msg);
            if (!string.IsNullOrEmpty(msg)) label1.Text = msg;

            //定时抓取jpeg图片
            CapTimer = new System.Timers.Timer();
            CapTimer.Interval = mCammeraMoniter.CaptureInterval;
            CapTimer.Enabled = true;
            CapTimer.Elapsed += (obj, ev) =>
            {
                this.BeginInvoke(
                    new Action(() =>
                    {
                        label1.Text = mCammeraMoniter.CaptureJpeg();
                        //label1.Text = System.DateTime.Now.ToString();
                    }));
            };
            CapTimer.Start();
        }

        public void RealDataCallBack(Int32 lRealHandle, UInt32 dwDataType, ref byte pBuffer, UInt32 dwBufSize, IntPtr pUser)
        {        }

        private void Prview_FormClosed(object sender, FormClosedEventArgs e)
        {
            mCammeraMoniter.StopPreview();
            string msg = "";
            mCammeraMoniter.LogOut(ref msg);
            mCammeraMoniter.Dispose();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCCapture
{
    public class FtpClient
    {
        public string IP { get; set; }

        public string UID { get; set; }

        public string PWD { get; set; }

        public string Path { get; set; }

        //private static FtpClient _instance = null;

        //private static readonly object SynObject = new object();

        //private FtpClient()
        //{
        //    //_ftp = new FtpWeb(this.IP, "", this.UID, this.PWD);
        //}

        //public static FtpClient Instance
        //{
        //    get
        //    {
        //        if (null == _instance)
        //        {
        //            lock (SynObject)
        //            {
        //                if (null == _instance)
        //                {
        //                    _instance = new FtpClient();
        //                }
        //            }
        //        }
        //        return _instance;
        //    }
        //}


        private FtpWeb _ftp;

        public FtpWeb Ftp
        {
            get
            {
                if (null == _ftp)
                {
                    _ftp = new FtpWeb(this.IP, "", this.UID, this.PWD);
                }
                return _ftp;
            }
        }

    }
}

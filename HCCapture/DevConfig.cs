﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HCCapture
{
    public class DevConfig
    {
        private static int _captureInterval;
        public static int CaptureInterval
        {
            get
            {
                return _captureInterval * 1000;
            }
            set
            {
                _captureInterval = value;
            }
        }

        public static string IntervalInfo
        {
            get
            {
                return _captureInterval + " 秒";
            }
        }

        public static string CapJpgPath { get; set; }

        public static List<DeviceInfo> devList { get; set; }

        public static FtpClient ftpClient { get; set; }

        private static SaveMode _mode;
        public static SaveMode Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
            }
        }

        public static void LoadConfig()
        {
            //CapJpgPath = getXmlValue("","");
            XDocument config = XDocument.Load(@".\config.xml");
            XElement root = config.Element("Config");

            CapJpgPath = root.Element("CapJpgPath").Value;
            if (string.IsNullOrEmpty(CapJpgPath))
                CapJpgPath = @".\";

            Enum.TryParse<SaveMode>(Convert.ToString(root.Element("Mode").Value), out _mode);

            XElement ftpEle = root.Element("Ftp");
            ftpClient = new FtpClient()
            {
                IP = ftpEle.Attribute("ip").Value,
                UID = ftpEle.Attribute("uid").Value,
                PWD = ftpEle.Attribute("pwd").Value,
                Path = ftpEle.Attribute("Path").Value
            };

            CaptureInterval = root.Element("CaptureInterval") == null ? 0 : Convert.ToInt32(root.Element("CaptureInterval").Value);

            var md = from d in config.Descendants("Devlist").Elements("Dev")
                     select new DeviceInfo()
                     {
                         ID = d.Attribute("id").Value,
                         IP = d.Attribute("ip").Value,
                         PORT = ushort.Parse(d.Attribute("port").Value),
                         UID = d.Attribute("uid").Value,
                         PWD = d.Attribute("pwd").Value,
                         PresetIndex = Convert.ToUInt16(d.Attribute("presetIndex").Value ?? "-1"),
                         PrePresetIndex = Convert.ToUInt16(d.Attribute("prePresetIndex").Value ?? "-1"),
                         Channel = Convert.ToInt32(d.Attribute("channel").Value ?? "1"),
                     };
            devList = md.ToList<DeviceInfo>();

        }
    }
}

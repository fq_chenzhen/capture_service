﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCCapture
{
    public class CammeraBase
    {
        public bool Active { get; set; }
        public DeviceInfo DvInfo { get; set; }
        public string CapJpgPath { get; set; }

        public string MonthPath
        {
            get
            {
                return System.DateTime.Now.ToString("yyMM");
            }
        }

        public Int32 lLoginID = -1;

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="msg"></param>
        public void Initialize()
        {
            //判断图片抓取路径是否存在
            //按月存放图片防止一个目录下图片过多
            //this.MonthPath = System.DateTime.Now.ToString("yyMM");
            this.CapJpgPath = string.Format(@"{0}\{1}\{2}", this.CapJpgPath.TrimEnd(new char[] { '/', '\\' }), DvInfo.ID, this.MonthPath);
        }

        /// <summary>
        /// 创建本地目录
        /// </summary>
        public void CreateLocalPath()
        {
            if (DevConfig.Mode != SaveMode.仅FTP && !FileHelper.IsExistDirectory(this.CapJpgPath)) //只ftp上传本地不创建目录
                FileHelper.CreateDirectory(this.CapJpgPath);
        }

        /// <summary>
        /// 创建ftp目录
        /// </summary>
        /// <param name="ftp"></param>
        public void CreateFtpPath(FtpWeb ftp)
        {
            //根目录
            //FtpClient.Instance.Ftp.GotoDirectory("", true);
            //FtpWeb ftp = new FtpWeb(this.IP, "", this.UID, this.PWD);
            if (!ftp.DirectoryExist(DevConfig.ftpClient.Path))
                ftp.MakeDir(DevConfig.ftpClient.Path);
            ftp.GotoDirectory(DevConfig.ftpClient.Path, false);
            if (!ftp.DirectoryExist(DvInfo.ID))
                ftp.MakeDir(DvInfo.ID);
            ftp.GotoDirectory(DvInfo.ID, false);

            if (!ftp.DirectoryExist(this.MonthPath))
                ftp.MakeDir(this.MonthPath);
            ftp.GotoDirectory(this.MonthPath, false);
        }

    }
}

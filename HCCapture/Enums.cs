﻿
public enum SaveMode : short
{
    本地按日期 = -1,
    仅本地 = 0,
    仅FTP = 1,
    本地和FTP = 2
}
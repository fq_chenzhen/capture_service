﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCCapture
{
    public class DeviceInfo
    {
        public string ID { get; set; }

        public string UID { get; set; }

        public string PWD { get; set; }

        public string IP { get; set; }

        public ushort PORT { get; set; }

        public ushort PresetIndex { get; set; }

        public ushort PrePresetIndex { get; set; }

        public int Channel { get; set; }
    }
}

﻿using Infrastructure.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceCapture.Job
{
    public class WebserviceClient
    {
        private object[] _args;

        public WebserviceModel Model { get; set; }

        public WebServiceAgent Agent { get; set; }

        public WebserviceClient CombineParamter()
        {
            var margs = Model.Args;
            _args = new object[margs.Count];
            for (int i = 0; i < margs.Count; i++)
            {
                _args[i] = margs[i].Value;

                if (margs[i].Value.IsMatch(@"\[.+?\]"))
                {
                    _args[i] = System.DateTime.Now.ToString(margs[i].Value.Substring("[", "]"));
                }
            }

            return this;
        }

        public IEnumerable<dynamic> Invoke(string method)
        {
            var text = Agent.Invoke(method, _args).ToString();
            return text.JsonToModels<IEnumerable<dynamic>>();
        }
    }
}
